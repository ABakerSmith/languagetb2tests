# Language Engineering Tests

Includes tests for 

- Parsing:
    - Numerals
    - Identifiers (i.e. Function names and variable names)
    - Aexp 
    - Bexp
    - DecP
    - DecV 
    - Statments 
- Dynamic Semantics
- Mixed Semantics
- Static Semantics

Probably not all the tests will work out of the box, as I'm testing some functions specific to my project, however hopefully you can use some of the tests, or modify others. 

You'll need:

- Megaparsec (`cabal install megaparsec`)
- HSpec      (`cabal install hspec`)
- [Utility functions for testing Megaparsec parsers with Hspec](https://hackage.haskell.org/package/hspec-megaparsec). (`cabal install hspec-megaparsec`)