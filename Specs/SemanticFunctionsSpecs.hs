module Specs.SemanticFunctionsSpecs where

import Test.Hspec
import Text.Megaparsec hiding (State)
import Text.Megaparsec.String

import ProcParser
import SemanticFunctions
import DynamicSemantics
import Specs.SemanticTestHelper

main :: IO ()
main = hspec $ do
    emptyStateSpec
    aValSpec
    bValSpec
    updateSpec
    updateManySpec
    restoreSpec

emptyStateSpec :: Spec
emptyStateSpec = do
    describe "emptyState" $ do
        it "has 0 for default values" $ do
            emptyState "x" `shouldBe` 0
            emptyState "y" `shouldBe` 0
            emptyState "z" `shouldBe` 0

aValSpec :: Spec
aValSpec = do
    describe "aVal" $ do
        it "evaulates numerals" $ do
            aVal (N 5) testState `shouldBe` 5

        it "evaulates variable x" $ do
            aVal (V "x") testState `shouldBe` 1

        it "evaulates variable y" $ do
            aVal (V "y") testState `shouldBe` 2

        it "evaulates variable z" $ do
            aVal (V "z") testState `shouldBe` 3

        it "evaulates multiplication" $ do
            aVal (Mult (V "y") (N 5)) testState `shouldBe` 10

        it "evaulates chained multiplication" $ do
            aVal (Mult (Mult (N 2) (N 3)) (V "z")) testState `shouldBe` 18

        it "evaulates addition" $ do
            aVal (Add (N 2) (N 8)) testState `shouldBe` 10

        it "evaulates chained addition" $ do
            aVal (Add (Add (V "x") (V "y")) (V "z")) testState `shouldBe` 6

        it "evaulates subtraction" $ do
            aVal (Sub (N 2) (N 8)) testState `shouldBe` -6

        it "evaulates chained subtraction" $ do
            aVal (Sub (Sub (V "x") (V "y")) (V "z")) testState `shouldBe` -4

bValSpec :: Spec
bValSpec = do
    describe "bVal" $ do
        it "evaluates TRUE" $ do
            bVal TRUE testState `shouldBe` True

        it "evaulates FALSE" $ do
            bVal FALSE testState `shouldBe` False

        it "evaluates negation of true" $ do
            bVal (Neg TRUE) testState `shouldBe` False

        it "evaulates negation of false" $ do
            bVal (Neg FALSE) testState `shouldBe` True

        it "evaulates FALSE & FALSE" $ do
            bVal (And FALSE FALSE) testState `shouldBe` False

        it "evaulates FALSE & TRUE" $ do
            bVal (And FALSE TRUE) testState `shouldBe` False

        it "evaulates TRUE & FALSE" $ do
            bVal (And TRUE FALSE) testState `shouldBe` False

        it "evaulates TRUE & TRUE" $ do
            bVal (And TRUE TRUE) testState `shouldBe` True

        it "evaulates TRUE & TRUE & FALSE" $ do
            bVal (And (And TRUE TRUE) FALSE) testState `shouldBe` False

        it "evaulates <= to be true" $ do
            bVal (Le (V "y") (N 6)) testState `shouldBe` True

        it "evaulates <= to be false" $ do
            bVal (Le (N 7) (V "z")) testState `shouldBe` False

        it "evaulates == to be true" $ do
            bVal (Eq (V "x") (N 1)) testState `shouldBe` True

        it "evaulates == to be false" $ do
            bVal (Eq (V "x") (V "y")) testState `shouldBe` False

updateSpec :: Spec
updateSpec = do
    describe "update" $ do
        it "updates the returned value" $ do
            update "x" 5 testState "x" `shouldBe` 5

        it "does not change the value of non-updated variables" $ do
            update "x" 5 testState "y" `shouldBe` 2
            update "x" 5 testState "z" `shouldBe` 3

updateManySpec :: Spec
updateManySpec = do
    describe "updateMany" $ do
        it "updates the returned values" $ do
             updateMany [("x", 5), ("y", 6)] testState "x" `shouldBe` 5
             updateMany [("x", 5), ("y", 6)] testState "y" `shouldBe` 6

        it "does not change the value of non-updated variables" $ do
            updateMany [("x", 5), ("y", 6)] testState "z" `shouldBe` 3

restoreSpec :: Spec
restoreSpec = do
    describe "restoreVars" $ do
        it "updates the returned values" $ do
            restore newS ["a", "b"] oldS "a" `shouldBe` 5
            restore newS ["a", "b"] oldS "b" `shouldBe` 6

        it "does not change the value of non-updated variables" $ do
            restore newS ["a"] oldS "b" `shouldBe` 2 where

        newS :: State
        newS "a" = 1
        newS "b" = 2

        oldS :: State
        oldS "a" = 5
        oldS "b" = 6
