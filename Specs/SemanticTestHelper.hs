module Specs.SemanticTestHelper where

import ProcParser
import SemanticFunctions

-- A state in which "x" evaulates to 1, "y" evaulates to 2 and "z"
-- evaulates to 3. All other variables evaulate to 0.
testState :: State
testState "x" = 1
testState "y" = 2
testState "z" = 3
testState _   = 0

-- A program used to test that the statement evaulators correctly with different
-- scope rules.
scopeStm :: Stm
scopeStm = Block [("x",N 0)] [("p",Ass "x" (Mult (V "x") (N 2))),("q",Call "p")] (Block [("x",N 5)] [("p",Ass "x" (Add (V "x") (N 1)))] (Comp (Call "q") (Ass "y" (V "x"))))

-- A program that is used to test the scope rules. "y" should evaluate to 0
-- after execution. 
scopeStmFail :: Stm
scopeStmFail = Block [("x",N 0), ("y", N 0)] [("p",Ass "x" (Mult (V "x") (N 2))),("q",Call "p")] (Block [("x",N 5)] [("p",Ass "x" (Add (V "x") (N 1)))] (Comp (Call "q") (Ass "y" (V "x"))))

-- A looping factorial program, that populates the variable "y" with "x"!.
facLoop :: Stm
facLoop = (Comp (Ass "y" (N 1)) (While (Neg (Eq (V "x") (N 1))) (Comp (Ass "y" (Mult (V "y") (V "x"))) (Ass "x" (Sub (V "x") (N 1))))))

-- A recursive factorial program, that populates the variable "y" with "x"!.
facCall :: Stm
facCall = (Block [] [("fac",Block [("z",V "x")] [] (If (Eq (V "x") (N 1)) Skip (Comp (Comp (Ass "x" (Sub (V "x") (N 1))) (Call "fac")) (Ass "y" (Mult (V "z") (V "y"))))))] (Comp (Ass "y" (N 1)) (Call "fac")))

-- A mututally recursive program to determine if the value of "x" is odd or
-- even. If "x" is even, "y" is populated with 0, otherwise "y" is populated
-- with 1. The value of `x` is also set to 0.
mutualEvenOdd :: Stm
mutualEvenOdd = Block [] [("isEven",If (Eq (V "x") (N 0)) (Ass "y" (N 0)) (Comp (Ass "x" (Sub (V "x") (N 1))) (Call "isOdd"))),("isOdd",If (Eq (V "x") (N 0)) (Ass "y" (N 1)) (Comp (Ass "x" (Sub (V "x") (N 1))) (Call "isEven")))] (Call "isEven")

-- A program used to test the function `stmVars` returns the correct set of
-- variables.
allVariablesStm :: Stm
allVariablesStm = Block [("a",Add (V "b") (V "c")),("d",V "e")] [("y",Ass "f" (V "g"))] (If (Eq (V "h") (V "i")) (While (Eq (V "j") (N 0)) Skip) (Comp (Comp (Comp (Ass "k" (N 0)) (Ass "l" (Add (V "m") (V "n")))) (Call "z")) Skip))
