module Specs.StaticSemanticSpecs where

import Test.Hspec
import Text.Megaparsec hiding (State)
import Text.Megaparsec.String

import ProcParser
import SemanticFunctions
import StaticSemantics
import Specs.SemanticTestHelper

-- A program used to test Skip, the inital value of `x` is 10.
skipTestStm :: Stm
skipTestStm = Block [("x",N 10)] [] Skip

-- A program used to test assignment. The initial value of `x` is 10, `y` is 5,
-- and `z` is 3. `x` should then be set to 2.
assTestStm :: Stm
assTestStm  = (Comp (Comp (Ass "y" (N 5)) (Ass "z" (N 3))) (Ass "x" (N 2)))

main :: IO ()
main = hspec $ do
    addToStoreSpec
    addToStateSpec
    updateStateSpec
    aexpVarsSpec
    bexpVarsSpec
    stmVarsSpec
    makeEnvStoreSpec
    sStaticSpec

addToStoreSpec :: Spec
addToStoreSpec = do
    describe "addToStore" $ do
        it "returns the location the variable was stored" $ do
            loc1 `shouldBe` 0
            loc2 `shouldBe` 1
            loc3 `shouldBe` 2

        it "returns the variable at locations" $ do
            sto store1 0 `shouldBe` 10
            sto store2 1 `shouldBe` 11
            sto store3 2 `shouldBe` 12 where
                (loc1, store1) = addToStore emptyStore 10
                (loc2, store2) = addToStore store1     11
                (loc3, store3) = addToStore store2     12

addToStateSpec :: Spec
addToStateSpec = do
    describe "addToState" $ do
        it "updates the environment and store" $ do
            makeState env3 store3 "x" `shouldBe` 10
            makeState env3 store3 "y" `shouldBe` 20
            makeState env3 store3 "z" `shouldBe` 30 where
                (env1, store1) = addToState "x" 10 emptyEnvV emptyStore
                (env2, store2) = addToState "y" 20 env1 store1
                (env3, store3) = addToState "z" 30 env2 store2

updateStateSpec :: Spec
updateStateSpec = do
    describe "updateState" $ do
        it "does not change the value of variables not specified" $ do
            makeState env5 store5 "z" `shouldBe` 30

        it "updates the environment and store" $ do
            makeState env5 store5 "x" `shouldBe` 11
            makeState env5 store5 "y" `shouldBe` 22 where
                (env1, store1) = addToState "x" 10 emptyEnvV emptyStore
                (env2, store2) = addToState "y" 20 env1 store1
                (env3, store3) = addToState "z" 30 env2 store2
                (env4, store4) = updateState "x" 11 env3 store3
                (env5, store5) = updateState "y" 22 env4 store4

aexpVarsSpec :: Spec
aexpVarsSpec = do
    describe "aexpVars" $ do
        it "returns the names of variables in an Aexp" $ do
            aexpVars (Sub (Add (V "x") (V "y")) (N 1)) `shouldBe` ["x", "y"]

        it "returns the empty list if there are no variables" $ do
            aexpVars (Mult (N 1) (N 2)) `shouldBe` []

bexpVarsSpec :: Spec
bexpVarsSpec = do
    describe "bexpVars" $ do
        it "returns the names of variables in a Bexp" $ do
            bexpVars (Le (V "a") (V "b")) `shouldBe` ["a", "b"]

        it "returns the empty list if there are no variables" $ do
            bexpVars (And TRUE FALSE) `shouldBe` []

stmVarsSpec :: Spec
stmVarsSpec = do
    describe "stmVars" $ do
        it "returns the names of the variables in an Stm" $ do
            stmVars allVariablesStm `shouldBe` (map (:[]) ['f'..'n'])

        it "returns the empty list if there are no variables" $ do
            stmVars (Comp Skip (Call "f")) `shouldBe` []

makeEnvStoreSpec :: Spec
makeEnvStoreSpec = do
    describe "makeEnvStore" $ do
        it "gives the correct inital values in the environment and store" $ do
            makeState env store "x" `shouldBe` 1
            makeState env store "y" `shouldBe` 2
            makeState env store "z" `shouldBe` 3 where
                (env, store) = makeEnvStore testState (Comp (Ass "x" (N 0)) (Comp (Ass "y" (N 0)) (Ass "z" (N 0))))

sStaticSpec :: Spec
sStaticSpec = do
    describe "s_static" $ do
        it "skips" $ do
            s_static skipTestStm emptyState "x" `shouldBe` 10

        it "assigns" $ do
            s_static assTestStm emptyState "x" `shouldBe` 2
            s_static assTestStm emptyState "y" `shouldBe` 5
            s_static assTestStm emptyState "z" `shouldBe` 3

        it "performs s1 if IF predicate is true" $ do
            s_static (If TRUE (Ass "x" (N 5)) (Ass "x" (N 0))) emptyState "x" `shouldBe` 5

        it "performs s2 if IF predicate is false" $ do
            s_static (If FALSE (Ass "x" (N 5)) (Ass "x" (N 0))) emptyState "x" `shouldBe` 0

        it "does composition" $ do
            s_static (Comp (Ass "x" (N 5)) (Ass "y" (N 6))) emptyState "x" `shouldBe` 5
            s_static (Comp (Ass "x" (N 5)) (Ass "y" (N 6))) emptyState "y" `shouldBe` 6

        it "does not perform the body of the while loop of the condition is false" $ do
            s_static (While FALSE (Ass "x" (N 5))) testState "x" `shouldBe` 1

        it "does while loops" $ do
            s_static (While (Neg (Eq (V "z") (N 0))) (Ass "z" (Sub (V "z") (N 1)))) testState "z" `shouldBe` 0

        it "correctly evaulates program" $ do
            s_static scopeStm emptyState "y" `shouldBe` 5

        it "correctly resets variables" $ do
            s_static scopeStmFail emptyState "y" `shouldBe` 0

        it "evaulates fac loop" $ do
            s_static facLoop (update "x" 1 emptyState) "y" `shouldBe` 1
            s_static facLoop (update "x" 2 emptyState) "y" `shouldBe` 2
            s_static facLoop (update "x" 3 emptyState) "y" `shouldBe` 6
            s_static facLoop (update "x" 4 emptyState) "y" `shouldBe` 24
            s_static facLoop (update "x" 5 emptyState) "y" `shouldBe` 120

        it "evaulates with self-recursion" $ do
            s_static facCall (update "x" 1 emptyState) "y" `shouldBe` 1
            s_static facCall (update "x" 2 emptyState) "y" `shouldBe` 2
            s_static facCall (update "x" 3 emptyState) "y" `shouldBe` 6
            s_static facCall (update "x" 4 emptyState) "y" `shouldBe` 24
            s_static facCall (update "x" 5 emptyState) "y" `shouldBe` 120

        it "evaulates with mutual-recursion" $ do
            s_static mutualEvenOdd (update "x" 0 emptyState) "y" `shouldBe` 0
            s_static mutualEvenOdd (update "x" 1 emptyState) "y" `shouldBe` 1
            s_static mutualEvenOdd (update "x" 2 emptyState) "y" `shouldBe` 0
            s_static mutualEvenOdd (update "x" 3 emptyState) "y" `shouldBe` 1
            s_static mutualEvenOdd (update "x" 4 emptyState) "y" `shouldBe` 0
            s_static mutualEvenOdd (update "x" 10 emptyState) "x" `shouldBe` 0
